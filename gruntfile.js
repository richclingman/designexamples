module.exports = function(grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({
        less: {
            development: {
                options: {
                    // compress: true,
                    // yuicompress: true,
                    optimization: 2
                },
                files: {  // destination file and source file
                    "caremessage.org/app/css/mobile.css":
                        "caremessage.org/app/css/mobile.less"
                }
            }
        },
        watch: {
            styles: {
                files: ['**/css/**/*.less'], // which files to watch
                tasks: ['less'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.registerTask('default', ['less', 'watch']);
};
