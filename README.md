# Design Examples #

Adding responsive CSS to existing websites by pulling HTML from site, 
adding only three lines, and serving with new responsive CSS. 
In most cases CSS has not been cleaned up.

### How ###

* Copied "Generated HTML" using FireFox web developer extension
* Modified HTML only enough to add Meta and CSS tags
* Set up simple NODE.JS web server
* Using GRUNT to watch LESS files and generate new CSS each time the file is saved

```
# ... at top of head
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="http://[the-original-host-to-get-images-etc]/">
# ... after existing CSS
<link rel="stylesheet" href="http://[server-name]/css/mobile.css">
```

### Who do I talk to? ###

* Rich Clingman
